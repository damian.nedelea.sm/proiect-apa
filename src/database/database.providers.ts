import { driver, auth } from 'neo4j-driver';

export const databaseProviders = {
  provide: 'Neo4j',
  useFactory: () => {
    return driver('bolt://localhost:7687', auth.basic('neo4j', '0696dam'));
  },
};
