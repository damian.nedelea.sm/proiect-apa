import { Module } from '@nestjs/common';
import { databaseProviders } from './database.providers';
import { ConfigService } from '../config/config.service';
import { ConfigModule } from '../config/config.module';

@Module({
  imports: [],
  exports: ['Neo4j'],
  providers: [databaseProviders],
})
export class DatabaseModule {}
