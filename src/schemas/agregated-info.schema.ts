import { Schema } from 'mongoose';

export interface AggregatedInfo extends Document {
    totalSum: number;
    directCount: number;
    indirectSum: number;
    indirectCount: number;
    totalCount: number;
    directSum: number;
}

export const AggregatedInfoSchema = new Schema({
    totalSum: { type: Number },
    directCount: { type: Number },
    indirectSum: { type: Number },
    indirectCount: { type: Number },
    totalCount: { type: Number },
    directSum: { type: Number },
});
