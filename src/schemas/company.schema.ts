import { Document, Schema } from 'mongoose';
import { AggregatedInfo, AggregatedInfoSchema } from './agregated-info.schema';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Company extends Document {
  address: string;
  name: string;
  oldId: string;
  aggregatedInfo: AggregatedInfo;
  founders: string[];
  administrators: string[];
  identity: number;
  normalizedName: string;
  status: string;
  date: number;
  type: string;
}

export const CompanySchema = new Schema({
  address: { type: String },
  name: { type: String },
  oldId: { type: String },
  aggregatedInfo: AggregatedInfoSchema,
  founders: [
    { type: Schema.Types.ObjectId, ref: 'Person' },
  ],
  foundersRaw: String,
  administratorsRaw: String,
  administrators: [
    { type: Schema.Types.ObjectId, ref: 'Person' },
  ],
  identity: { type: Number },
  normalizedName: { type: String },
  date: { type: Number },
  status: { type: String },
  type: { type: String },
});

CompanySchema.plugin(mongoosePaginate);
