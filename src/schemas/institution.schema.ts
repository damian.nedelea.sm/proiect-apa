import { Document, Schema } from 'mongoose';
import { AggregatedInfo, AggregatedInfoSchema } from './agregated-info.schema';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface Institution extends Document {
  name: string;
  oldId: string;
  aggregatedInfo: AggregatedInfo;
}

export const InstitutionSchema = new Schema({
  name: { type: String },
  oldId: { type: String },
  aggregatedInfo: AggregatedInfoSchema,
});

InstitutionSchema.plugin(mongoosePaginate);
