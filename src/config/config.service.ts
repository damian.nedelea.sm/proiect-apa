import * as dotenv from 'dotenv';
import * as fs from 'fs';

export class ConfigService {
  NEO4J_URI: string;
  private readonly envConfig: { [key: string]: string };

  constructor() {
    if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
      this.envConfig = {
        NEO4J_URI: process.env.NEO4J_URI,
        NEO4J_DB: process.env.NEO4J_DB,
        NEO4J_PASS: process.env.NEO4J_PASS,
        PORT: process.env.PORT,
        ADMIN_EMAIL: process.env.ADMIN_EMAIL,
        ADMIN_PASSWORD: process.env.ADMIN_PASSWORD,
        JWT_SECRET: process.env.JWT_SECRET,
        MAILCHIMP_AUDIENCE_ID: process.env.MAILCHIMP_AUDIENCE_ID,
        MAILCHIMP_API_KEY: process.env.MAILCHIMP_API_KEY,
        SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
      };
    } else {
      this.envConfig = dotenv.parse(fs.readFileSync('.env'));
    }
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}
