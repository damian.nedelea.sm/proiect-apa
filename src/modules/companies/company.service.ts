import { Inject, Injectable } from '@nestjs/common';
import { Driver } from 'neo4j-driver';

// const companies = require('../../../data/company');

@Injectable()
export class CompanyService {
  constructor(
    @Inject('Neo4j') private neo4j: Driver,
  ) {
  }

  async CreateRelations() {
    let i = 0;
    const companies = await this.neo4j.session().run('MATCH (n:Company) RETURN n');
    console.log(companies.records);
    for (const company of companies.records) {
      //   i++;
      const id = company['_fields'][0].properties.oldId;
      const founders = company['_fields'][0].properties.foundersRaw.split(', ').join(',').split(',');
      const administrators = company['_fields'][0].properties.administratorsRaw.split(', ').join(',').split(',');

      for (const founder of founders) {
        await this.neo4j.session().run(
          `MATCH (a:Person),(b:Company)
                 WHERE a.name =~ '(?i)${founder}' AND b.oldId = '${id}'
                 CREATE (a)-[:FOUNDER]->(b)
                 RETURN a.name, b.name`,
        );
      }

      for (const administrator of administrators) {
        await this.neo4j.session().run(
            `MATCH (a:Person),(b:Company)
                 WHERE a.name =~ '(?i)${administrator}' AND b.oldId = '${id}'
                 CREATE (a)-[:ADMINISTRATOR]->(b)
                 RETURN a.name, b.name`,
          );
      }

      //   company.founders = await this.getAdministrationsIds(company.foundersRaw);
      //   company.administrators = await this.getAdministrationsIds(company.administratorsRaw);
      //   console.log(i);
      //   await this.neo4j.session().run(`
      //   MATCH (a:Artist),(b:Album)
      //   WHERE a.Name = "Strapping Young Lad" AND b.Name = "Heavy as a Really Heavy Thing"
      //   CREATE (a)-[r:RELEASED]->(b)
      //   RETURN r
      //   `, {Person: newCompany});
      //
    }
    return null;
  }

  async populateDatabase() {
    let i = 1;
    for (const oldCompaniesArray of []) {
      for (const oldCompany of oldCompaniesArray) {
        console.log(i);
        //     const founders = companies[0][0].foundersRaw.split(', ').join(',').split(',');
        // console.log(founders);
        // for (const founder of founders) {
        //   const newFounder = await this.personModel.findOne({name: { $regex: founder, $options: 'i' }});
        //   console.log(newFounder);
        // }
        const newCompany = {
          address: oldCompany.address,
          name: oldCompany.name,
          oldId: oldCompany.id,
          // aggregatedInfo: oldCompany.aggregatedInfo,
          foundersRaw: oldCompany.foundersRaw,
          founders: [],
          administratorsRaw: oldCompany.administratorsRaw,
          administrators: [],
          identity: oldCompany.identity,
          normalizedName: oldCompany.normalizedName,
          status: oldCompany.state,
          date: oldCompany.registrationDate,
          type: oldCompany.type,
        };
        await this.neo4j.session().run(`CREATE (:Company $Person)`, { Person: newCompany });
        i++;
      }
    }
    return null;
  }

  // async getAdministrationsIds(personString): Promise<string[]> {
  //   const personsStringArray = personString.split(', ').join(',').split(',');
  //   const personsArray = [];
  //   for (const person of personsStringArray) {
  //     let databaseData = await this.personModel.findOne({name: { $regex: person, $options: 'i' }});
  //     if (databaseData) {
  //       personsArray.push(databaseData);
  //     } else {
  //       databaseData = await this.companyModel.findOne({name: { $regex: person, $options: 'i' }});
  //       if (databaseData) {
  //         console.log(databaseData);
  //         personsArray.push(databaseData);
  //       } else {
  //         databaseData = await this.institutionModel.findOne({name: { $regex: person, $options: 'i' }});
  //         if (databaseData) {
  //           console.log(databaseData);
  //           personsArray.push(databaseData);
  //         }
  //       }
  //     }
  //   }
  //   return personsArray;
  // }
}
