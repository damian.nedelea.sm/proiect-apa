import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { CompanyService } from './company.service';
import { Person } from '../../schemas/person.schema';

@Controller('companies')
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}


  @Get('/create-relations')
  async CreateRelations(): Promise<void> {
    return this.companyService.CreateRelations();
  }

  @Get('/populate-database')
  async PopulateDatabase(): Promise<void> {
    return this.companyService.populateDatabase();
  }
}
