import { Module } from '@nestjs/common';
import { CompanyService } from './company.service';
import { CompanyController } from './company.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonSchema } from '../../schemas/person.schema';
import { CompanySchema } from '../../schemas/company.schema';
import { InstitutionSchema } from '../../schemas/institution.schema';
import { databaseProviders } from '../../database/database.providers';

@Module({
  imports: [
  ],
  providers: [CompanyService, databaseProviders],
  controllers: [CompanyController],
})
export class CompanyModule {}
