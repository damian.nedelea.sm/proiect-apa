import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonSchema } from '../../schemas/person.schema';
import { CompanySchema } from '../../schemas/company.schema';
import { InstitutionService } from './institution.service';
import { InstitutionSchema } from '../../schemas/institution.schema';
import { InstitutionController } from './institution.controller';
import { databaseProviders } from '../../database/database.providers';

@Module({
  imports: [
  ],
  providers: [InstitutionService, databaseProviders],
  controllers: [InstitutionController],
})
export class InstitutionModule {}
