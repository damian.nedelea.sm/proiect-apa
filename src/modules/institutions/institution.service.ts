import { Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Company } from '../../schemas/company.schema';
import { Person } from '../../schemas/person.schema';
import { Institution } from '../../schemas/institution.schema';
import { Driver } from 'neo4j-driver';

const institutions = require('../../../data/institution');

@Injectable()
export class InstitutionService {
  constructor(
    @Inject('Neo4j') private neo4j: Driver,
  ) {
  }

  async populateDatabase() {
    let i = 1;
    const length = institutions.length;
    for (const oldInstitutionsArray of institutions) {
      for (const oldInstitution of oldInstitutionsArray) {
        console.log(i);
        if (length === i || length === i - 1 || length === i + 1) {
          console.log('done');
        }
        const newInstitution = {
          oldId: oldInstitution.id,
          name: oldInstitution.name,
          aggregatedInfo: `[value in ${oldInstitution.agregatedInfo} | toString(value)]` ,
        };
        await this.neo4j.session().run('CREATE (:Institution $Institution)', {Institution: newInstitution});        i++;
      }
    }
    return null;
  }
}
