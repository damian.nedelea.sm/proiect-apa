import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { InstitutionService } from './institution.service';

@Controller('institutions')
export class InstitutionController {
  constructor(private readonly institutionService: InstitutionService) {}

  @Get('/populate-database')
  async PopulateDatabase(): Promise<void> {
    return this.institutionService.populateDatabase();
  }

}
