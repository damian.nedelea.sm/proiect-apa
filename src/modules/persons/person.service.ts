import { Inject, Injectable } from '@nestjs/common';
import { Driver } from 'neo4j-driver';

const persons = require('../../../data/person.json');

@Injectable()
export class PersonService {
  constructor(
    @Inject('Neo4j') private neo4j: Driver,
  ) {
  }

  async findAll(queryParams: any) {
    return this.neo4j.session().run('MATCH (n:Person) RETURN n LIMIT 5');
  }

  async populateDatabase() {
    let i = 1;
    for (const oldPersonArray of persons) {
      for (const oldPerson of oldPersonArray) {
        console.log(i);
        const newPerson = {
          oldId: oldPerson.id,
          name: oldPerson.name,
          aggregatedInfo: `[value in ${oldPerson.agregatedInfo} | toString(value)]` ,
        };
        await this.neo4j.session().run('CREATE (:Person $Person)', {Person: newPerson});
        i++;
      }
    }
    return null;
  }
}
