import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { PersonService } from './person.service';

@Controller('persons')
export class PersonController {
  constructor(private readonly personService: PersonService) {}

  @Get()
  async FindAllPersons(@Query() queryParams: any): Promise<any> {
    return this.personService.findAll(queryParams);
  }

  @Get('/populate-database')
  async PopulateDatabase(): Promise<void> {
    return this.personService.populateDatabase();
  }

}
