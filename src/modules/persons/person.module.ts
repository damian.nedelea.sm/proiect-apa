import { Module } from '@nestjs/common';
import { PersonController } from './person.controller';
import { PersonService } from './person.service';
import { DatabaseModule } from '../../database/database.module';
import { databaseProviders } from '../../database/database.providers';

@Module({
  imports: [],
  providers: [PersonService, databaseProviders],
  controllers: [PersonController],
})
export class PersonModule {}
