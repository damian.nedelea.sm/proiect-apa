import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigService } from './config/config.service';
import { CompanyModule } from './modules/companies/company.module';
import { PersonModule } from './modules/persons/person.module';
import { DatabaseModule } from './database/database.module';
import { InstitutionModule } from './modules/institutions/institution.module';

@Module({
  imports: [
    InstitutionModule,
    CompanyModule,
    PersonModule,
    DatabaseModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    ConfigService,
  ],
})
export class AppModule {}
